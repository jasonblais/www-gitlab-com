---
layout: markdown_page
title: "Category Direction - Integrations"
---

- TOC
{:toc}

| ------ | ------ |
| **Stage** | [Enablement](https://about.gitlab.com/direction/enablement) | 
| **Maturity** | [Viable](https://about.gitlab.com/handbook/product/categories/maturity/) |

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AIntegrations)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics/1515)
- [Overall Vision](/direction/ecosystem/)

_This direction is a work in progress, and [everyone can contribute](#contributing):_

* Please comment, thumbs-up (or down!), and contribute to the linked issues and epics on this category page. Sharing your feedback directly on GitLab.com is the best way to contribute to our vision.
* Please share feedback directly via [email](mailto:pdeuley@gitlab.com), [Twitter](https://twitter.com/gitlab) or on a video call. If you're a GitLab user and have direct knowledge of your need from a particular integration, we'd especially love to hear from you.

## Overview

GitLab's vision is to be the best [single application for every part of the DevOps toolchain](https://about.gitlab.com/handbook/product/single-application/). However, some customers use tools other than our built-in features--[and we respect those decisions](https://about.gitlab.com/handbook/product/#plays-well-with-others). The Integrations category was created specifically to better serve those customers.

Currently, GitLab offers [30+ project services](https://docs.gitlab.com/ee/user/project/integrations/project_services.html#project-services) that integrate with a variety of external systems. Integrations are a high priority for GitLab, and the *Integrations* category was established to develop and maintain these integrations with key 3rd party systems and services. 

## Direction

The **Integrations** direction is to support customers who integrate other tools with GitLab, and do our best to provide them with a great experience. As we add support for more integrations the number of people able to use GitLab will grow.

It is also increasingly important that GitLab serves the needs of larger customers, many of which rely on systems like Jira, Jenkins, and ServiceNow. It can be vitally important to have a robust integration with these services, and _not_ having that can make the experience painful, and in some cases block adoption of GitLab entirely.

By making these integrations powerful and useful, we make the lives of our users better--even when they're using other products. This is what we mean when we say that GitLab [plays well with others](https://about.gitlab.com/handbook/product/#plays-well-with-others).

## Maturity

The Integrations category additionally tracks [Maturity](https://about.gitlab.com/direction/maturity/) on a per-integration basis. Each integration is evaluated by the following criteria:

* A **Minimal** integration meets a single basic need for a small set of customers, and may only push data one way from one system to the other without surfacing much data or functionality directly in the UI.
* A **Viable** integration meets the core needs of most customers, and is robust or configurable enough to meet all the needs of a some customers.
* A **Complete** integration meets the needs of the vast majority of usecases for the majority of users, and the integration allows users to work painlessly between the two products.
* A **Lovable** integration not only meets the needs of the vast majority of users, but it makes the experience of using both products as productive and easy as possible. This may mean things like special consideration taken to intra-product navigation and how we surface notifications from the other service, for example.

## Current High-priority Integrations

_You can view a list of all of our current integrations on our [Project services documentation page](https://docs.gitlab.com/ee/user/project/integrations/project_services.html)_

| Integration           | Maturity Level    | Documentation                                                                         | Epic/Issue    |
| ---                   | ---               | ---                                                                                   | --- |
| Atlassian Jira        | Viable            | [Documentation](https://docs.gitlab.com/ee/user/project/integrations/jira.html)       | [Epic](https://gitlab.com/groups/gitlab-org/-/epics/1522) |
| Jenkins               | Viable            | [Documentation](https://docs.gitlab.com/ee/integration/jenkins.html)                  | [Epic](https://gitlab.com/groups/gitlab-org/-/epics/1757) |
| ServiceNow            | Planned           |                                                                                       | [Epic](https://gitlab.com/groups/gitlab-org/-/epics/1712) &middot; [MVC Issue](https://gitlab.com/gitlab-org/gitlab/issues/14717) |

## Prioritization

There are many services that could potentially integrate with GitLab and the DevOps space (and beyond.) Because of this, we prioritize the integrations we work on based on:

* Customer demand (Reach) -- We want work on what the majority of our customers need. We evaluate this based on issue activity, current usage data, and external market trends.
* Impact -- Integrations that are vital to a user's workflow that have many interaction points with GitLab also have the biggest opportunity to sink productivity if not integrated well.
* Integration complexity (Effort)

Based on these priorities, we're currently only targeting a limited set of products and services--specifically, those listed above and those that are [scheduled on our backlog](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AIntegrations&milestone_title=Any). All prioritized integrations have a target maturity of `Complete`, and will take precedent over other integrations until they hit that maturity level.

_For all other integrations, our team also supports our [API and SDKs]() so that developers can build anything else they can imagine._ Additionally, we have an Alliances team that's always happy to talk to any companies interested in [integrating their platform with GitLab](https://about.gitlab.com/partners/integrate/).

## What's next and why

### Improvements to Jira integration

This is our current top priority. There is currently a large backlog of requests for new features, bugs to be fixed, and documentation issues around our Jira integration. Of all the products we integrate with, this has by far the most customer interest, and we'd like GitLab to continue to integrate with Jira better than BitBucket does.

* [Group level integration with Jira](https://gitlab.com/gitlab-org/gitlab/issues/2519)
* [Improving Jira Integration Configuration](https://gitlab.com/groups/gitlab-org/-/epics/1691)
* [Improving the configuration of transition IDs](https://gitlab.com/gitlab-org/gitlab/issues/16119)
* [Porting Jira Connect App functionality to self-hosted customers](https://gitlab.com/groups/gitlab-org/-/epics/1626)

### Integrating ServiceNow

A number of our large customers have requested integration with ServiceNow, and we know how important these types of workflow management tools can be to how they operate their business. 

* [ServiceNow Integration MVC](https://gitlab.com/gitlab-org/gitlab/issues/14717) - _We're currently investigating requirements with this particular system, and we'd love to hear from you. You can contribute by detailing your use cases in the [ServiceNow Integration epic](https://gitlab.com/groups/gitlab-org/-/epics/1712), or in the [ServiceNow Integration MVC](https://gitlab.com/gitlab-org/gitlab/issues/14717). As this MVC progresses, we'll identify additional issues to be worked on and iterate over this feature. As they get added, they'll show up here._

### Improvements to Jenkins integration

Many firms are still using Jenkins for their CI/CD needs, and as much as they may want to migrate off of Jenkins, it can prove difficult. We have an Integrations Category working on solving that problem, but in the meanwhile we also need our product to play nice with Jenkins for those who can't move off of it.

* [Group-level Jenkins Integration](https://gitlab.com/gitlab-org/gitlab/issues/6757)

### Laying Groundwork for More Integrations

As we expanding our available integrations and work to make it easier for developers in our community to contribute their own, it's essential that we start to make many of those integration points reusable for new work to be added later. This will allow us to increase the velocity of our own feature additions of new integrations, as well as enable partners and community members to add the features they need most.

* [Add usage ping telemetry for integrations](https://gitlab.com/gitlab-org/gitlab/issues/31134)
* [Design: Embedding content in Issues](https://gitlab.com/gitlab-org/gitlab/issues/25924)
* [Create bridge for chat platforms](https://gitlab.com/gitlab-org/gitlab/issues/16711)
* [Extensible API to provision managed Kubernetes Clusters](https://gitlab.com/gitlab-org/gitlab/issues/25008)

## Contributing

At GitLab, one of our values is that everyone can contribute. If you're looking to
contribute your own integration, or otherwise get involved with features in the Ecosystem area, [you can find open issues here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AIntegrations).

You can read more about our general contribution guidelines [here](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).

## Feedback & Requests

If there's an integration that you'd like to see GitLab offer, or if you have feedback about an existing integration: please [submit an issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/new?issue%5Btitle%5D=New%20GitLab%20Integration%20with) with the label `~Category:Integrations`, or contact [Patrick Deuley](mailto:pdeuley@gitlab.com), Sr. Product Manager, Ecosystem.
